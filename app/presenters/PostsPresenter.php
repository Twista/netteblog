<?php
/**
 * Description of PostsPresenter.php
 * @author Twista
 * @package 
 */

use Nette\Application\UI\Form;

class PostsPresenter extends BasePresenter{

    /** @var var PostModel */
    protected $model;

    /** @var CommentModel */
    protected $comments;

    /**
     * @param PostModel $model
     */
    public function injectPostModel(PostModel $model){
        $this->model = $model;
    }

    /**
     * @param CommentModel $com
     */
    public function injectCommentModel(CommentModel $com){
        $this->comments = $com;
    }

    //
    // RENDER METODY
    //

    /**
     * render all posts
     */
    public function renderAll(){
        $this->template->posts = $this->model->findAll();
    }

    /**
     * render single post
     * @param int $id
     */
    public function renderDetail($id){
        $this->template->post = $this->model->findById($id);
    }

    //
    // COMMENT FORM
    //

    /**
     * create comment form whith
     * @return \Nette\Application\UI\Form
     */
    public function createComponentCommentFrom(){
        $form = new Form();
        $form->getElementPrototype()->class = 'ajax';

        $form->addText('name','Jméno')
            ->setRequired('Vyplňte vaše jméno');

        $form ->addTextArea('text','Příspěvek')
            ->setRequired('Vyplňte text příspěvku')
            ->addRule(Form::MIN_LENGTH,'Minimální délka příspěvku je %d znaků',10);

        $form->addText('email','E-Mail')
            ->addCondition(Form::FILLED)
            ->addRule(Form::EMAIL,'Vyplňte validní email');

        $form->addHidden('post_id',$this->getParam('id'));

        $form->addSubmit('send','Odeslat odpověď');

        $form->onSuccess[] = $this->proccessComment;

        return $form;
    }

    /**
     * @param \Nette\Application\UI\Form $form
     */
    public function proccessComment(Form $form){
        // vytvoření komentáře
        $values = $form->getValues();
        $this->comments->insertComment($values);

        // notifikace a přesměrování
        $this->flashMessage('Komentář byl přidán','success');
        if($this->isAjax()){
            $this->invalidateControl();
        } else{
            $this->redirect('this');
        }

    }

}