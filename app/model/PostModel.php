<?php
/**
 * Description of PostModel.php
 * @author Twista
 * @package 
 */

class PostModel extends Nette\Object {

    protected $table;

    /**
     * @param \Nette\Database\Connection $conn
     */
    public function __construct(Nette\Database\Connection $conn){
        $this->table = $conn->table('posts');
    }

    /**
     * @return \Nette\Database\Table\Selection
     */
    public function findAll(){
        return $this->table;
    }

    /**
     * find one row by id
     * @param int $id
     *
     * @return \Nette\Database\Table\ActiveRow
     */
    public function findById($id){
        return $this->table->get($id);
    }
}